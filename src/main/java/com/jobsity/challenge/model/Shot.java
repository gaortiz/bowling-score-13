package com.jobsity.challenge.model;

import com.jobsity.challenge.enums.ScoreType;

public class Shot {

    private String name;
    private Integer value;

    protected Shot(String name, Integer value) {
        this.name = name;
        this.value = value;
    }

    public static Shot createShot(String name) {
        if (name.equals(ScoreType.FOUL.inputValue)) {
            return createFoul();
        } else {
            return new Shot(name, Integer.parseInt(name));
        }
    }

    private static Shot createFoul() {
        return new Shot(ScoreType.FOUL.label, ScoreType.FOUL.value);
    }

    public boolean isStrike() {
        return name.equals(ScoreType.STRIKE.inputValue);
    }

    public boolean isSpare(Shot nextShot) {
        return this.value + nextShot.value == ScoreType.SPARE.value;
    }

    public static Shot createEmptyShot() {
        return new Shot(ScoreType.EMPTY_SHOT.label, ScoreType.EMPTY_SHOT.value);
    }

    public String getName() {
        return name;
    }

    public Integer getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Shot{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
